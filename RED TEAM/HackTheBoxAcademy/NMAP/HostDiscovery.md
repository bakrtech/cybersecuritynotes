# Host Enumeration
## Host Discovery
```
sudo nmap {ip range eg: 192.100.1.0/24} -sn -oA {file name} | grep "for" | cut -d" " -f5
```
* This command scans and gives a list of which hosts are up
* This helps so that we can only scan specified hosts
* The command broken down is :
    * `sudo nmap` is used to scan network with super previlages 
    * `{IP range}` is the ip range given which we need to scan
    * `-sn` is used to tell that we don't need to scan the ports
        * This will use `ICMP ECHO REQUEST` to scan instead of default `TCP SYN` 
    * `-oA {file name}` is used to save the output in all supported output file formats
    * `grep "for" | cut -d" " -f5` is used to have a clean output where we only see the ips of the hosts 
## Scaning IP lists
```
nmap -sn -oA {filen name} -iL {file name of List of ips }
```
* This command helps us to find which hosts are up in a given list 
* `-iL {file name of list of ips}` is used to give the name of  file which has list of ips
## Scanning multiple ips
```
namp {flags } {ip one } {ip two } {ip three} ......
```
or
**Take an exmple where we need to scan 1.1.1.1 , 1.1.1.2 , 1.1.1.3 ,1.1.1.4**
```
nmap {flags} 1.1.1.1-4 
```
## Some additinal flags
* `-PE` is used to tell to use `ICMP ECHO REQUEST`
* `--Packet-trace ` is used to see full track of the packet sent 
* `--disable-arp-ping` is used to disable arp ping 
