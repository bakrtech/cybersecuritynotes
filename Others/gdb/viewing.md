- In GDB (GNU Debugger), the x command (short for examine) is used to inspect memory and data in various formats. It allows you to examine the contents of memory locations, which can be crucial for debugging and understanding how your program interacts with memory. Here are some common formats and options used with x in GDB:

## Syntax of x Command

- The basic syntax of the x command is:

`x/[count][format] address`
`[count]` : Optional. Specifies the number of units to display.
`[format]`: Optional. Specifies the format for displaying the data.
`address`: Required. Specifies the memory address or expression to examine.

## Common Formats and Options

- Data Formats:
  `x`: Examine as memory (hexadecimal).
  `d`: Examine as signed decimal integer.
  `u`: Examine as unsigned decimal integer.
  `o`: Examine as octal integer.
  `t`: Examine as binary (two's complement).

- Size Modifiers:
  `b`: Byte (8 bits).
  `h`: Halfword (16 bits).
  `w`: Word (32 bits).
  `g`: Giant word (64 bits).

- String Format:
  `s`: Examine as C string (null-terminated).

- Repetition Count:
  Specifies how many units to display.

## Examples

- Examine memory as hexadecimal (x) and as integer (d):

```
gdb

(gdb) x/4xw &variable // Examine 4 words of memory starting from address of variable
(gdb) x/16xb 0x1000 // Examine 16 bytes of memory starting from address 0x1000
(gdb) x/d &integer // Examine integer at address of integer
```

- Examine memory as C string (s):

```
gdb

(gdb) x/s 0x2000 // Examine memory at address 0x2000 as C string
```

- Repetition count:

```
gdb

    (gdb) x/10xw &array    // Examine 10 words of memory starting from address of array
```

- Additional Options

  - Display format width:

```
    gdb

(gdb) x/10xw &array // Display 10 words of memory in hexadecimal (w: word size)

```

    * Specify format:

```
gdb

(gdb) x/10xb &buffer // Display 10 bytes of memory in hexadecimal (b: byte size)
```

    * Examine struct or complex data types:

```
gdb

    (gdb) x/10i $pc         // Display 10 instructions at current instruction pointer ($pc)
```

## Summary

- The x command in GDB is powerful for examining memory contents in various formats, helping debug and understand how data is stored and manipulated in your program. Understanding these options and formats will enhance your ability to use GDB effectively for debugging C and C++ programs.
