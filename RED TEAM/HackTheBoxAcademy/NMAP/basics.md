# NMAP
## Basic Syntax
```
nmap {Scan Type } {options} {target} 
```
## Scaning Technique
  * `-sS/sT/sA/sW/sM` is used for  TCP SYN/Connect()/ACK/Window/Maimon scans
  * `-sU` is used for UDP Scan
  * `-sN/sF/sX` is used for TCP Null, FIN, and Xmas scans
  * `--scanflags <flags>` is used to  Customize TCP scan flags
  * `-sI <zombie host[:probeport]>` is used to do a  Idle scan
  * `-sY/sZ` is used for  SCTP INIT/COOKIE-ECHO scans
  * `-sO` is used for IP protocol scan
  * `-b <FTP relay host>` is used for FTP bounce scan
## `-sS` scan 
* In this case Nmap sends a packet with `SYN` flag to the port and if :
    * It recives a `SYN-ACK` flag then the Port is **OPEN**
    * It recives a `RST` flag then the Port is is **CLOSED**
    * It doesn't recives a flag then the Port is **FILTERED** which means the packet is droped or unable to get due to firewall 
