# Nmap
## Basic scan 
```
nmap {ip}
```
* This scan just scans the ip with the ports upto 1000 and does the most basic
   scan
* In genral nmap shows 3 coloumn
```
PORT STATE SERVICES 
```
   * Port shows the number of the port which is scaned and the Protocol used by it 
   * State shows wether the port is open , close or filtered (firewall in place)
   * Services shows the application that is running on that port 
## Script and version scan 
```
nmap -sV -sC -p- {ip}
```
* The `-sV` flag shows the version of the application running
* The `-sC` flag runs an appropriate script according the application running 
* The `-p-` scans all the ports

