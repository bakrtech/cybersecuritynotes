# 4.1 Ethernet
## 4.1.1 The Rise of Ethernet
* In the early days of networking, each vendor used its own proprietary methods of interconnecting network devices and networking protocols. If you bought equipment from different vendors, there was no guarantee that the equipment would work together. Equipment from one vendor might not communicate with equipment from another.
* As networks became more widespread, standards were developed that defined rules by which network equipment from different vendors operated. Standards are beneficial to networking in many ways:
	-   Facilitate design
	-   Simplify product development
	-   Promote competition
	-   Provide consistent interconnections
	-   Facilitate training
	-   Provide more vendor choices for customers
* There is no official local area networking standard protocol, but over time, one technology, Ethernet, has become more common than the others. Ethernet protocols define how data is formatted and how it is transmitted over the wired network. The Ethernet standards specify protocols that operate at Layer 1 and Layer 2 of the OSI model. Ethernet has become a de facto standard, which means that it is the technology used by almost all wired local area networks, as shown in the figure.
* ![[Pasted image 20230515104855.png]]
## 4.1.2 Ethernet Evolution
* "The Institute of Electrical and Electronics Engineers, or IEEE ", maintains the networking standards, including Ethernet and wireless standards. IEEE committees are responsible for approving and maintaining the standards for connections, media requirements and communications protocols. Each technology standard is assigned a number that refers to the committee that is responsible for approving and maintaining the standard. The committee responsible for the Ethernet standards is 802.3.
* Since the creation of Ethernet in 1973, standards have evolved for specifying faster and more flexible versions of the technology. This ability for Ethernet to improve over time is one of the main reasons that it has become so popular. Each version of Ethernet has an associated standard. For example, 802.3 100BASE-T represents the 100 Megabit Ethernet using twisted-pair cable standards. The standard notation translates as:
	-   100 is the speed in Mbps
	-   BASE stands for baseband transmission
	-   T stands for the type of cable, in this case, twisted-pair.
* Early versions of Ethernet were relatively slow at 10 Mbps. The latest versions of Ethernet operate at 10 Gigabits per second and more. Imagine how much faster these new versions are than the original Ethernet networks.
## 4.1.3 VIDEO Ethernet Addressing
**VIDEO**