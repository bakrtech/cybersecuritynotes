# Get-wmiobject
## Getting basic info about os 
* Input
```
Get-wmiobject -Class Win32_OperatingSystem 
```
* Output
```
SystemDirectory : C:\Windows\system32                                                                                                         
Organization    :                                                                                                                                                 
BuildNumber     : 19045                                                                                                                                            
RegisteredUser  : admin                                                                                                                                                    
SerialNumber    : 00330-80000-00000-AA278                                                                                                                               
Version         : 10.0.19045
```
## Getting info about all the running process
* Input
```
Get-wmiobject -Class win32_process
```
* Output
```
__GENUS                    : 2                                                                                                                                  
__CLASS                    : Win32_Process                                                                                                                  
__SUPERCLASS               : CIM_Process                                                                                                                          
__DYNASTY                  : CIM_ManagedSystemElement                                                                                                             
__RELPATH                  : Win32_Process.Handle="768"                                                                                                           
__PROPERTY_COUNT           : 45                                                                                                                                   
__DERIVATION               : {CIM_Process, CIM_LogicalElement, CIM_ManagedSystemElement}                                                                          
__SERVER                   : DESKTOP-079HN5D                                                                                                                      
__NAMESPACE                : root\cimv2                                                                                                                           
__PATH                     : \\DESKTOP-079HN5D\root\cimv2:Win32_Process.Handle="768"                                                                              
Caption                    : lsass.exe                                                                                                                            
CommandLine                :                                                                                                                                      
CreationClassName          : Win32_Process                                                                                                                        
CreationDate               : 20230604065057.563159+060                                                                                                            
CSCreationClassName        : Win32_ComputerSystem                                                                                                                 
CSName                     : DESKTOP-079HN5D                                                                                                                      
Description                : lsass.exe                                                                                                                            
ExecutablePath             :                                                                                                                                      
ExecutionState             :                                                                                                                                      
Handle                     : 768                                                                                                                                  
HandleCount                : 1224                                                                                                                                 
InstallDate                :                                                                                                                                      
KernelModeTime             : 9843750                                                                                                                              
MaximumWorkingSetSize      :                                                                                                                                      
MinimumWorkingSetSize      :                                                                                                                                      
Name                       : lsass.exe                                                                                                                            
OSCreationClassName        : Win32_OperatingSystem                                                                                                                
OSName                     : Microsoft Windows 10 Pro|C:\Windows|\Device\Harddisk0\Partition3                                                                     
OtherOperationCount        : 5950                                                                                                                                 
OtherTransferCount         : 996678                                                                                                                               
PageFaults                 : 7652                                                                                                                                 
PageFileUsage              : 7104                                                                                                                                 
ParentProcessId            : 592                                                                                                                                  
PeakPageFileUsage          : 7368                                                                                                                                 
PeakVirtualSize            : 2203420565504                                                                                                                        
PeakWorkingSetSize         : 19100                                                                                                                                
Priority                   : 9                                                                                                                                    
PrivatePageCount           : 7274496                                                                                                                              
ProcessId                  : 768                                                                                                                                  
QuotaNonPagedPoolUsage     : 25                                                                                                                                   
QuotaPagedPoolUsage        : 148                                                                                                                                  
QuotaPeakNonPagedPoolUsage : 28                                                                                                                                   
QuotaPeakPagedPoolUsage    : 151                                                                                                                                  
ReadOperationCount         : 468                                                                                                                                  
ReadTransferCount          : 52734                                                                                                                                
SessionId                  : 0                                                                                                                                    
Status                     :                                                                                                                                      
TerminationDate            :                                                                                                                                      
ThreadCount                : 8                                                                                                                                    
UserModeTime               : 13437500                                                                                                                             
VirtualSize                : 2203415846912                                                                                                                        
WindowsVersion             : 10.0.19045                                                                                                                           
WorkingSetSize             : 19267584                                                                                                                             
WriteOperationCount        : 315                                                                                                                                  
WriteTransferCount         : 168081                                                                                                                               
PSComputerName             : DESKTOP-079HN5D                                                                                                                      
ProcessName                : lsass.exe                                                                                                                            
Handles                    : 1224                                                                                                                                 
VM                         : 2203415846912                                                                                                                        
WS                         : 19267584                                                                                                                             
Path                       :            
```
**NOTE: The Output contains only one process but it acctually lists all the process**
## Getting information about running services 
* Input
```
Get-wmiobject -Class win32_service
```
* Output
```
ExitCode  : 1077                                                                                                                                                         
Name      : AJRouter                                                                                                                                                    
ProcessId : 0                                                                                                                                                           
StartMode : Manual                                                                                                                                                       
State     : Stopped                                                                                                                                                      
Status    : OK    
```
**NOTE: The Output contains only one service but it acctually lists all the process**
## Getting the information about the BIOS (Basic Input output system)
* Input
```
Get-wmiobject -Class win32_Bios
```
* Output
```
SMBIOSBIOSVersion : unknown                                                                                                                                        
Manufacturer      : EDK II                                                                                                                                         
Name              : unknown                                                                                                                                        
SerialNumber      :                                                                                                                                                
Version           : BOCHS  - 1
```
## Other classes 
* This command is used to get list of all the classes of WMI object
```
Get-wmiobject -List 
```
