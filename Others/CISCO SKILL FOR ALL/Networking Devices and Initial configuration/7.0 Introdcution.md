# 7.0 Introduction
## 7.0.1 Webster Why Should I take this module ?
* Webster here again! I have another friend to introduce to you. Her name is Olcay. Olcay is an IT professional at a power company in Turkey. She is mentoring a new hire named Abay. Abay will be shadowing Olcay for the next month to become more proficient in networking in the power company. Olcay asks Abay what he knows about address resolution. Abay knows that to send a packet to another host on the same local IPv4 network, a host must know the IPv4 address and the MAC address of the destination device. A device uses ARP to determine the destination MAC address of a local device when it knows its IPv4 address.
* If Abay is going to be successful at his new job, he needs to learn a little more and so do you! I suggest taking this module on Address Resolution.
## 7.0.2 What Will I learn in this Module ?
* ARP
