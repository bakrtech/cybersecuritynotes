# FOOT PRINTING
## Objectives
* Foot printing concepts
* foot printing through search engines and google Hacking techniques 
* foot printing through web services and networking sites
* whois, DNS and network footprinting 
* footprinting through social engineering 
* Use different footprinting tools
## What is footprinting 
* Footprinting, the first step in ethical hacking, refers to the process of collecting information about a target network and its environment. 
* Using footprinting, you can find a number of opportunities to penetrate and assess the target organization’s network. 
* There is no single methodology for footprinting, as information can be traced in a number of ways.
## Types of Footprinting 
* Passive
* Active
## Information obtained in Footprinting
* Organization Information
* Network Information
* System Information
## Objectives of footprinting 
* To build a hacking strategy, attackers must gather information about the target organization’s network
* They then use such information to identify the easiest way to break through the organization's security perimeter.
* As mentioned previously, the footprinting methodology makes it easy to gather information about the target organization and plays a vital role in the hacking process. 
## Footprinting Threats
* Social Engineering
* System and network attacks
* information leakage
* Privacy loss
* Corporate Espionage
* Business Loss
## Footprinting methodology
* The footprinting methodology is a procedure for collecting information about a target organization from all available sources. 
* It involves gathering information about a target organization, such as URLs, locations, establishment details, number of employees, specific range of domain names, contact information, and other related information.
![[Pasted image 20240611140143.png]]
## Footprinting through search engines
*  Search engines use automated software, i.e., crawlers, to continuously scan active websites and add the retrieved results in the search engine index that is further stored in a massive database.
*  Many search engines can extract target organization information such as technology platforms, employee details, login pages, intranet portals, contact information, and so on.
## Footprinting using Advanced google Hacking techniques 
* Google hacking refers to the use of advanced Google search operators for creating complex search queries to extract sensitive or hidden information.
* Advanced Google hacking refers to the art of creating complex search engine queries. Queries can retrieve valuable data about a target company from Google search results.
* The syntax to use an advanced search operator is as follows `operator: search_term`
* **Note**: Do not enter any spaces between the operator and the query. 
* some of the operators are : 
	* `site:` This operator restricts search results to the specified site or domain. 
	* `allinurl:` This operator restricts results to only the pages containing all the query terms specified in the URL.
	* `inurl:` This operator restricts the results to only the pages containing the specified word in the URL
	* `allintitle:` This operator restricts results to only the pages containing all the query terms specified in the title
	* `intitle:` This operator restricts results to only the pages containing the specified term in the title
	* `allinanchor:` This operator restricts results to only the pages containing all query terms specified in the anchor text on links to the pages
	* `cache:` This operator displays Google's cached version of a web page instead of the current version of the web page.
	* `link:` This operator searches websites or pages that contain links to the specified website or page.
	* `related:` This operator displays websites that are similar or related to the URL specified.
	* `info:` This operator finds information for the specified web page.
	* `location:` This operator finds information for a specific location.
	* `filetype:` This operator allows you to search for results based on a file extension.
## What a hacker can do with the google hacking 
* An attacker can create complex search-engine queries to filter large amounts of search results to obtain information related to computer security.
* Examples of sensitive information on public servers that an attacker can extract with the help of Google Hacking Database (GHDB) queries include:
	* Error messages that contain sensitive information 
	* Files containing passwords 
	* Sensitive directories 
	* Pages containing logon portals 
	* Pages containing network or vulnerability data, such as IDS, firewall logs, and configurations
	* Advisories and server vulnerabilities 
	* Software version information 
	* Web application source code 
	* Connected IoT devices and their control panels, if unprotected  
	* Hidden web pages such as intranet and VPN services
