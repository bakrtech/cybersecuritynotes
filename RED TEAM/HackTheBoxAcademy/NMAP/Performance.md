# Performance
* Scaning performance plays a vital role 
* Some of the flags that can be used to performance:
    * `-T<1-5>` sets the speed
    * `--min-parallelisim <number>` sets frequency
    * `--max-rtt-timeout <time>` sets timeout 
    * `--min-rate <number>` how many packages to sent each time
    * `--max-retires <number> ` how much time to set within each packet until retires
## Timeout 
* By default the Nmap scan starts with with very hight timeout i.e 100ms 
* If we want to optimise the scan we can the intital timeout to 50ms and then max to 100ms

* Default scan  
```
 sudo nmap 10.129.2.0/24 -F
```
Total time spent on scan = 39.44 seconds

* Optimised scan 
```
sudo nmap 10.129.2.0/24 -F --initial-rtt-timeout 50ms --max-rtt-timeout 100ms
```
Total time spent on scan = 12.29 seconds

*THOUGH THE SPEPED IS INCREASED BUT THE QUALITY OF SCAN IS REDUCED AS THIS SHOWS LESS PORTS THEN ABOVE*
## Retires
* By default the the retry value is set 10 

* Default Scan 
```
 sudo nmap 10.129.2.0/24 -F | grep "/tcp" | wc -l
```
 Output = 23 ports scaned

* Optimised scan
```
sudo nmap 10.129.2.0/24 -F --max-retries 0 | grep "/tcp" | wc -l
```
Output = 21 ports 

* *The optimised scan took less time but the quality degraded*
## Rates
* This could be set with `--min-rate` flag where we can set how much packet we sent in specific time
* *This is the only flag which when increased would **NOT** effect the quality of output*
## Timing
* Though the last one but the most important one
* This adjust the network trafic according the time we set 
* `-T 0` being the sneakest and polite on network and `-T 5` being the most agreesive on network i.e overloads the network
* *They don't change how output but the agrissiveness of the output*
* A list of is below : 
    * `-T 0 / -T paranoid`
    * `-T 1 / -T sneaky`
    * `-T 2 / -T polite`
    * `-T 3 / -T normal`
    * `-T 4 / -T aggressive`
    * `-T 5 / -T insane`
* By default the agrrisiveness is set to `-T 3 `
