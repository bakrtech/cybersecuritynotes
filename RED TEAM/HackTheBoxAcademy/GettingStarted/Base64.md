# BASE64
## Encoding command
```
base64 {file name } -w 0
```
* This command will enode the file in base64  
* `-w` is a wrap flag 
## Decoding command
```
echo "{encoded code }" | base64 -d > {file name}
```
* This command will decode the base64 code back to normal words using the `-d` flag 

**This command is very important in places where you can't copy the file to your
machine**
