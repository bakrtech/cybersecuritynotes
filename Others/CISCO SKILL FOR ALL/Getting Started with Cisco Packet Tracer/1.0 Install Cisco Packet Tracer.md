# 1.0 Install CIsco Packet Tracer
## 1.0.1 Video - Welcome to Getting started with CISCO packet tracer
**VIDEO**
## 1.0.2 First time in course
* **First Time in this Course**  
* The Getting Started with Cisco Packet Tracer course is designed for new users of the Cisco Packet Tracer network simulation and visualization tool. Whether you are a student with no networking knowledge, a student with some background in networking, or a student currently enrolled in a course that requires the use of Packet Tracer, this course will get you started!
* The free Cisco Packet Tracer software helps you to practice your network configuration and troubleshooting skills using your desktop or laptop computer. It allows you to simulate networks without having access to physical equipment. In addition to networking, you can learn, and then practice your Internet of Things (IoT) and Cybersecurity skills. You can choose to build a network from scratch, use a pre-built sample network, or complete lab assignments. While Packet Tracer is not a replacement for practicing on physical routers, switches, firewalls, and servers, it provides many benefits!
* This self-paced course will help you to become familiar with the Packet Tracer software that is used in many other Cisco Networking Academy and Skills for All courses. The course focuses on exploring the Packet Tracer interface, including menus, icons, and devices that are used in networking.
* **Resources Available to You**  
* Skills for All frequently asked questions can be found at [https://skillsforall.com/help](https://skillsforall.com/help), or by clicking Help in the top navigation bar. For Packet Tracer downloading help, visit [https://skillsforall.com/resources/lab-downloads](https://skillsforall.com/resources/lab-downloads) and scroll down to Learning Resources.
* You can find additional FAQs by visiting our virtual assistant, Morgan. Click the chat icon in the top right corner to choose from a list of topics or enter your question. Morgan’s help includes information about Packet Tracer, certificates, and badging.
## 1.0.3 Download Cisco Packet Tracer
* To obtain and install your copy of Cisco Packet Tracer, please follow the instructions from the link below:[https://skillsforall.com/resources/lab-downloads](https://skillsforall.com/resources/lab-downloads) 
## 1.0.4 Why Should I take this course
* What if you could see inside of a small business network or the internet? Have you ever wanted to set up an IoT system to alert you by phone when there is a problem in your home environment? Welcome to Cisco Packet Tracer – the simulation environment that can help you do all that and more.
* The Getting Started with Cisco Packet Tracer course is designed to familiarize you with the Cisco Packet Tracer network simulation and visualization tool.
* The course has two modules. In the first module, Download and Use Cisco Packet Tracer, you will learn to install the software and explore the interface. In addition to text, there are seven videos, and a hands-on exploration activity.
* After you are familiar with the Cisco Packet Tracer interface, you will take the second module, Create a Cisco Packet Tracer Network. You will create your own network in Packet Tracer (PT). You will also learn about the different types of PT files. This module has five videos and one Packet Tracer activity.
* **Let's get started with Cisco Packet Tracer!**
## 1.0.5 Video - Overview 
**Video**
