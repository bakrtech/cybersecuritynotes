#!/bin/bash 
if [ "$1" == "" ]
then
echo "ERROR: Missing the ip "
echo "Syntax: ipsweep <IP_hostpart>"
else
for ip in `seq 1 254`;do 
 ping -c 1 $1.$ip | grep "64 bytes" | cut -d" " -f4 | sed "s/://g" & 
done
fi
