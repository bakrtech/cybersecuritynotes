import sys,socket 
from datetime import datetime

# Checking if the ip was given or not also if the ip is corrrect or not
if len(sys.argv) !=2:
    print("ERROR: Invalid input !!\nSyntax python <scriptname>.py [ip | hostname]")
    exit()
try:
    target =socket.gethostbyname(sys.argv[1]) # if we provide the hostname instead of ip like google.com instead of 1.1.1.1 then this command would translate ti to that 
except:
    print("sorry some error occured while translating input to ip \nexiting!!!!\nERROR: Invalid input !!\nSyntax python <scriptname>.py [ip | hostname]")
    exit()
print("─"*50+"\nScanning target: "+target+"\nStarted at : "+str(datetime.now())+"\n"+"─"*50)
try:
    for port in range(50,100):
        s =socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        socket.setdefaulttimeout(1)
        result = s.connect_ex((target,port))
        if result ==0:
            print(f"{port} is open ")
        else:
            print(f"{port} is closed ")
        s.close()

except KeyboardInterrupt:
    print("\n","─"*50,"\nEXITING..........")
    sys.exit()
except socket.error:
    print("unable to connect to the given ip ")
    sys.exit()
