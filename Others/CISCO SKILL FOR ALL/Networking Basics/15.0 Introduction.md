# 15.0 Introduction
## 15.0.2 Webster Why Should I take this Module ?
* Kishori arrives at work early to get on a video conference call on the desktop computer at her nursing station. She logs into the session about mask protocol at the hospital. As she intently listens to the presenter, she notices a few dropped words. She wonders if it is a problem with the network. Is this similar to her tablet losing the connection for a moment? But then she remembers that she is using a computer that is hardwired to the network.
* Immediately after the call, she sends an email to Madhav in the IT department. Madhav comes to Kishori’s desk. She is confused because all of the devices do seem to be connected. Madhav explains that UDP and TCP are transport layer protocols that operate a little differently. He tells her that UDP is a 'best effort' delivery system that does not require acknowledgment of receipt. UDP is preferable with applications such as streaming audio and VoIP. UDP is used for video conference calls.
* Kishori had not heard of this before. Have you? In this module you will compare these protocols. Keep reading!
## 15.0.2 What Will I learn in this Module / 
* TCP and UDP 
* Port numbers
