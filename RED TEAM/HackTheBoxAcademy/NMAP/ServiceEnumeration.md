# Service Enumaration
## Service version Detection 
* `-sV` flag is used for service detection scan 
* `--stats-every=<time>s/m` is optioanl flag which is used to get stats about scan in about specified time in `m` for min or `s` for seconds
* `-v` or `-vv` is also an optional flag used to set the verbosity of the output depending on flag (`-v` being low and `-vv` being higher)
## Banner Grabing
* Banner refers to the deatails of the target for eg OS , Service running etc..
* This could be done by various methods:
    * ` sudo nmap 10.129.2.28 -p- -sV -Pn -n --disable-arp-ping --packet-trace` is using the above Version scanning flag and some others to disable the unnecary scans
    * `nc -nv 10.129.2.28 25` is using nc  for getting info

* `sudo tcpdump -i eth0 host 10.10.14.2 and 10.129.2.28` can be used to monitor the TCP network on a host 
* What it looks on network when using `TCP DUMP`
```
18:28:07.128564 IP 10.10.14.2.59618 > 10.129.2.28.smtp: Flags [S], seq 1798872233, win 65535, options [mss 1460,nop,wscale 6,nop,nop,TS val 331260178 ecr 0,sackOK,eol], length 0
```
* Sends a `SYN` flag 
```
18:28:07.255151 IP 10.129.2.28.smtp > 10.10.14.2.59618: Flags [S.], seq 1130574379, ack 1798872234, win 65160, options [mss 1460,sackOK,TS val 1800383922 ecr 331260178,nop,wscale 7], length 0
```
* Sends a `ACK-SYN` flag 
```
18:28:07.255281 IP 10.10.14.2.59618 > 10.129.2.28.smtp: Flags [.], ack 1, win 2058, options [nop,nop,TS val 331260304 ecr 1800383922], length 0
```
* Sends a `ACK` flag 
```
18:28:07.319306 IP 10.129.2.28.smtp > 10.10.14.2.59618: Flags [P.], seq 1:36, ack 1, win 510, options [nop,nop,TS val 1800383985 ecr 331260304], length 35: SMTP: 220 inlane ESMTP Postfix (Ubuntu)
```
* Sends a `PSH` flag
* This is wehre our details lie 
```
18:28:07.319426 IP 10.10.14.2.59618 > 10.129.2.28.smtp: Flags [.], ack 36, win 2058, options [nop,nop,TS val 331260368 ecr 1800383985], length 0
```
* Sends a `ACK` flag 

