# Windows Command
**NOTE: THESE COMMANDS ARE FOR CMD(COMMAND LINE)**
## seeing all the files and folders in a directory
```
dir {folder name for eg: c:\} /a 
```
* This command will show all the files and folders with the Date of last modification  , time of last modification and the size if it is file orther wiseshows shows `<DIR>,<JUNCTION> etc...`
    * `/a` is used to tell that we want to see all the files 
## Tree Output of a folder 
```
tree {directory name}
```
* Shows the tree of the folder 
## Integrity Control Access Control List 
```
icacls {Path to the file }
```
* This command shows us  permision level of different folders
* These are the different resoure level flag :
    * `(CI)` Container Inherit
    * `(OI)` Object Inherit
    * `(IO)` Inherit only
    * `(NP)` Do no propagate inherit 
    * `(I)`  Permision inherited from parent container 
* Differnt permision flags are :
    * `F` Full Access
    * `D` Delete Access
    * `N` No Access
    * `M` Modify acccess
    * `RX` Read and execute acceess
    * `R` Read only Access
    * `W` write only Access
## checking which services are running
```
Get-Service | ? {$_.Status -eq "Running"} | select -First 2 |fl 
```
* This command shows top 2 running services
    * `Get-Service` is used to get list of running services
    * `? {$_.Status -eg "Running"}` is used to select only those services which are running
    * `select -First 2 ` is used to get top 2 services in the list
    * `fl`  is a command used to format list
## Getting Information of a service 
```
sc qc {Service name}
```
* This command gives us the Information of the service 
## getting info of a service running on network
```
sc //{hostname or ip of the box } {qurey} {Service name}
```
* Same as above just on network
## Starting or stoping service using `sc`
```
sc start {service name}
```
```
sc stop {service name}
```

