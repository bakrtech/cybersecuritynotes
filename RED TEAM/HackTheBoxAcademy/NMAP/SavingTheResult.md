# Saving the results
## Different Formats
* We can save the output in different types of file depending on our uses
    * `-oN` is used to get output in `.nmap` file which  is a normal output 
    * `-oG` is used to get output in `.gnmap` file which is grepable output
    * `-oX` is used to get output in `.xml` file gets the file in .xml file format 
    * `-oA` is used to get output in all the above file formats 
* The file genrated are if given names is tes for example:
    ```
    test.nmap
    test.gnmap
    test.xml
    ```
## Stylesheets 
* The xml file given can be easyly used to create a HTML file with the help of `xsltproc`
* The command is :
```
xsltproc target.xml -o target.html
```
