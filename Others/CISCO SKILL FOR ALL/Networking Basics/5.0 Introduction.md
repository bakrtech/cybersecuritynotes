# 5.0 Introduction
## 5.0.1 Webster Why Should I Take this Module
* The next day, Kishori has a new patient, Srinivas, who has just been admitted to a room. He is from Narayanpet and speaks Telugu. Kishori speaks Marathi. These two Indian languages are very different. Kishori and Srinivas do not speak each other's native language. However, they do both speak English. Therefore, they decide to communicate using English.
* Before beginning to communicate with each other, we establish rules or agreements to govern the conversation. Just like Kishori and Srinivas, we decide what method of communication we should use, and what language we should use. We may also need to confirm that our messages are received. For example, Kishori may have Srinivas sign a document verifying that he has understood Kishori’s care instructions.
* Networks also need rules, or protocols, to ensure successful communication. This module will cover the communication principles for networks. Let's get started!
## 5.0.2 What Will I learn in this Module ?
* Communication Protocols
* Communication Standards
* Network Communication Models
