# Get-Acl 
# Basic execution
* **ACL** stands for **A**cces **C**ontrol **L**ist
* Input
```powershell
Get-Acl --path {path to the directory} | fl
```
* Output 
```
Path   : Microsoft.PowerShell.Core\FileSystem::C:\Users\admin\Desktop\
Owner  : DESKTOP-079HN5D\admin
Group  : DESKTOP-079HN5D\None
Access : NT AUTHORITY\SYSTEM Allow  FullControl
         BUILTIN\Administrators Allow  FullControl   
         DESKTOP-079HN5D\admin Allow  FullControl
Audit  :
Sddl   : O:S-1-5-21-194798022-4235037295-3962473795-1001G:S-1-5-21-194798022-4235037295-3962473795-513D:(A;OICIID;FA;;;SY)(A;OICIID;FA;;;BA)(A;OICIID;FA;;;S-1-5-21-194798022-4235037295-3962473795-1001)     
```
* This is a small but very usefull command as it lists the **ACCESS** and **SDDL**(Security Descriptor Defination Language ) of a particular directory 

