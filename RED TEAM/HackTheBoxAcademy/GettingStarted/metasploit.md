# Metasploit 
## Launching it     
* To to run it type `mfsconsole` in terminal 
## Searching a exploit 
```
search exploit {exploit name}
```
* This command in msfconsole will search for the exploit related to that name 
* The output contains
```
# Name DisclosureDate Rank Check Description
```
## Using a exploit 
```
use {path to the exploit }
```
* To use a exploit just type use and then the path to the exploit 
## Seeing the neccary options to configure the exploit
```
show options 
```
* This command after selecting the exploit shows us the different options we
need to configure 
## Setting up the values for exploit
```
set {Argument} {value}
```
* This command will set value to that Argument eg: ```set RHOSTS 10.10.10.20 ```
## Checking if the target is vulnerable to that exploit
```
check 
```
* This command checks that the target is vulnerable to that exploit 
## Running the exploit 
```run ``` or ```exploit```
* This command will run that exploit and starts exploiting that target 

