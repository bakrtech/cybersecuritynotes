# 1.2 Hierarchical Network Design
## 1.2.1  Physical and Logical Address
* A person's name usually does not change. A person's address on the other hand, relates to where the person lives and can change. On a host, the MAC address does not change; it is physically assigned to the host NIC and is known as the physical address. The physical address remains the same regardless of where the host is placed on the network.
* The IP address is similar to the address of a person. It is known as a logical address because it is assigned logically based on where the host is located. The IP address, or network address, is assigned to each host by a network administrator based on the local network.
* IP addresses contain two parts. One part identifies the network portion. The network portion of the IP address will be the same for all hosts connected to the same local network. The second part of the IP address identifies the individual host on that network. Within the same local network, the host portion of the IP address is unique to each host, as shown in the figure.
* Both the physical MAC and logical IP addresses are required for a computer to communicate on a hierarchical network, just like both the name and address of a person are required to send a letter.
* ![[Pasted image 20230514061937.png]]
## 1.2.2 Video View Network Information on my device
**VIDEO**
## 1.2.3 Lab  View Wireless and Wired NIC information
**pracs**
## 1.2.4 Hierarchical Analogy
* Imagine how difficult communication would be if the only way to send a message to someone was to use the person's name. If there were no street addresses, cities, towns, or country boundaries, delivering a message to a specific person across the world would be nearly impossible.
* On an Ethernet network, the host MAC address is similar to a person's name. A MAC address indicates the individual identity of a specific host, but it does not indicate where on the network the host is located. If all hosts on the internet (millions and millions of them) were each identified by their unique MAC address only, imagine how difficult it would be to locate a single one.
* Additionally, Ethernet technology generates a large amount of broadcast traffic in order for hosts to communicate. Broadcasts are sent to all hosts within a single network. Broadcasts consume bandwidth and slow network performance. What would happen if the millions of hosts attached to the internet were all in one Ethernet network and were using broadcasts?
* For these two reasons, large Ethernet networks consisting of many hosts are not efficient. It is better to divide larger networks into smaller, more manageable pieces. One way to divide larger networks is to use a hierarchical design model.
## 1.2.5 Video Benifits of a Hirerarchical Network design 
**VIDEO**
## 1.2.6 Access , Distribution and Core
* IP traffic is managed based on the characteristics and devices associated with each of the three layers of the hierarchical network design model: Access, Distribution and Core.
* Description of each layer 
	* Access Layer
		* The access layer provides a connection point for end user devices to the network and allows multiple hosts to connect to other hosts through a network device, usually a switch, such as the Cisco 2960-XR shown in the figure, or a wireless access point. Typically, all devices within a single access layer will have the same network portion of the IP address.
		* If a message is destined for a local host, based on the network portion of the IP address, the message remains local. If it is destined for a different network, it is passed up to the distribution layer. Switches provide the connection to the distribution layer devices, usually a Layer 3 device such as a router or Layer 3 switch.
		* ![[Pasted image 20230514064048.png]]
	* Distribution Layer
		* The distribution layer provides a connection point for separate networks and controls the flow of information between the networks. It typically contains more powerful switches, such as the Cisco C9300 series shown in the figure, than the access layer as well as routers for routing between networks. Distribution layer devices control the type and amount of traffic that flows from the access layer to the core layer.
		* ![[Pasted image 20230514064208.png]]
	* Core Layer
		* The core layer is a high-speed backbone layer with redundant (backup) connections. It is responsible for transporting large amounts of data between multiple end networks. Core layer devices typically include very powerful, high-speed switches and routers, such as the Cisco Catalyst 9600 shown in the figure. The main goal of the core layer is to transport data quickly.