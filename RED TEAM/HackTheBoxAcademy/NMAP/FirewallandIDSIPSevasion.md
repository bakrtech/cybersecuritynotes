# Firewall and IDS / IPS evasion
## firewall
* It is a security measure against unauthorized connections
* It monitors the network b/w the firewall and incoming traffic
* It checks the packet and decide if has to be padssed / ignored or blocked
* It is design to prevent unwanted connectons which could be potentaily dangerous
## IDS /IPS
* Intrusion Detecion system (IDS) and Intrusion prevention system (IPS)
* They scan the network for pontetial attacks , annalyzes them and reports them 
* IDS detets the intrusions and IPS adds the methods to prevent them by blocking etc..
## Determine Firewall and Their Rules 
* When a packet is shown as `FILTERED` it can have sevral meaning
* The packet can either be droped orj rejected
    * The droped packet means that the packet was ignored by firewall and thus droped so no response was seen 
    * The rejected packet means the the packet returned with a `RST` Flag of ICMP error code 
* Some of the error 
    * Net unreachable 
    * Net Prohibited 
    * Host Unreachable 
    * Host Prohibited
    * Port Unreachable 
    * Proto Unreachable 
* The Nmap's TCP ACK scan is much harder to detect as it shows that the conncetion is already established and it's a response whreas in the case of SYN scan it shows that the host is trying to create a new connection.
## Detect IDS / IPS
* Unlike the firewalls the IDS are not that easy to crack as they monitors the packet and notifes admin about 
* IPS  takes the measure by itslef configured by admins 
* IDS and IPS are different applications and IPS are complimant on IDS i.e IDS are main thing and we could say that IPS is a plugin for IDS
* During these process it's recomended that we use multiple VPS ( Virtual Private Servers ) so that Our IPs can't be detected
* If we do it from same IP then the admin would come to know that we our doing somehing wrong and then he would block our IP stoping us from accesing the network in first place and then if our ISP is contacted then whole Internet connections would be lost
    * IDS alone are usually there to help the admins detect potentail attacks on their network , We can check wheter they are present by triggering the alarms for eg we aggrivesivly scan a network and see that if ther any measure for same
    * another way to see that IDS / IPS are present is to scan from a particular VPS and see that if that VPS is blocked from network and if admin has take note of it
* Through this we can see that if we need to do a quite scan and from which alarms we have to take care of 
## Decoys 
* Some time admins, IPS block certain ips , subnets or even region to prevent access from certain networks
* Thsi method could be handled by DECOYS .
* it inserts random IPS into the IP header so that the reciver is unable to detect which is the original source IP
* These servers should be present at the time scan so that the server would be unavialable due to SYN-Flood security mecahnisim
* This could be done by using `-D RND:<number>`
* if we want to specify the source IP we can use `-S` flag for it 
