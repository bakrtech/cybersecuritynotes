# GoBuster
## Basic scan
```
gobuster dir -u {urlwithprotocol eg: https://192.168.10.1} -w {wordlist}
```
* This is a basic gobuster scan which provided with links can scan on for dirs
and file s on a web application
    * `dir` flag is used to tell that we are searching for directory
    * `-u` flag is used to give the url of the webapp 
    * `-w` flag is used to give the wordlist of the comman dir names
* Some of the comman web codes are: 
    * `200` resource's request was succesfull
    * `403` resource's was forbidden
    * `301` resource redirect 
## DNS subdomain Enumeration 
```
gobuster dns -d {domain name} -w {wordlist}
```
* This scan searchs for dirs and files not only on the domain you provided i.e
the url but also the subdomains related with it 
    * `dns` flag is used to tell that we are searching for subdomains 
    * `-d` flag is used to tell the domain 
    * `-w` flag is used to give the worldist of the comman subdomain names 

