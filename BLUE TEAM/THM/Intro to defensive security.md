# Intro to Defensive Security
[Link to the room](https://tryhackme.com/room/defensivesecurityhq)
## Introduction to Defensive Security
### Defensive security is somewhat the opposite of offensive security
### It has two main tasks
* Preventing intrustion from occurring 
* Detecting intrusions when they occur and responding properly
### Blue teams are part of the defensive security landscape
### Some of tasks related to defensive security are
* User Cyber Security awareness
* Documenting and managing assets
* Updating and patching Systems
* Setting up preventive security devices
* Setting up logging and monitoring devices
## Areas of Defensive Security
#### We will cover 
* Security Operations Center (SOC), where we cover Threat Intelligence
* Digital Forensics and Incident Response (DFIR), where we also cover Malware Analysis
### Security Operation Center ( SOC )
#### A security Operations Center ( SOC ) is a team of cyber securityi professionals that monitors the network and its systems to detect malicious cyber security events 
#### Some of the areas are: 
* Vulnerablites
* Policy voilations 
* Unauthorized activity 
* Network intrusions
#### Threat Intelligence
* *Intelligence refers to information you gather about actual and potential enemies
* *Threat is any action that can disrupt or adversely affect a system
* The purpose would be to achieve a threat-informed defense
* Based on the company (target), we can expect adversaries.
* ![[Pasted image 20230325195355.png]]
* Intelligence needs data 
* As a result of threat intelligence, we identify the threat actor (adversary), predict their activity, and consequently, we will be able to mitigate their attacks and prepare a response strategy.
### Digital Forensics and Incident Response (DFIR)
#### We will cover: 
* Digital forensics
* Incildent Response 
* Malware analysis
#### Digital Forensics
* Forensics is the application of science to investigate crimes and establish facts
* With the use and spread of digital systems, such as computers and smartphones, a new branch of forensics was born to investigate related crimes: computer forensics, which later evolved into, digital forensics.
* digital forensics will focus on different areas such as:
	* File system 
	* System memory
	* System logs
	* Network Logs
#### Incident Response
* An *incident*  usually refers to a data breach or cyber attack; however, in some cases, it can be something less critical, such as a misconfiguration, an intrusion attempt, or a policy violation
* *Incident response* specifies the methodology that should be followed to handle such a case. The aim is to reduce damage and recover in the shortest time possible. Ideally, you would develop a plan ready for incident response.
* The four major phases of incident response are :
	* Preparation
	* Detection and Analysis
	* Containment, Eradication and recovery
	* Post-Incident Activity
* ![[Pasted image 20230327164838.png]]
#### Malware Analysis
* Malware stands for malicious software
* Differnt types of malwares are :
	* Virus
	* Trojan horse
	* Ransomeware
* Malware analysis aims to learn about such malicious programs using various means:
	* _Static analysis_ works by inspecting the malicious program without running it. Usually, this requires solid knowledge of assembly language (processor’s instruction set, i.e., computer’s fundamental instructions).
	* _Dynamic analysis_ works by running the malware in a controlled environment and monitoring its activities. It lets you observe how the malware behaves when running.
## Practical Example of Defensive security
### Purely Practical Just Do it 
