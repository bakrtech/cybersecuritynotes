# Host and Port scanning
## Specifying the number of ports to be scaned
* If nothing is specified then it scan top 1000 ports 
* `-p {Number of port }` scans a specified port 
* `-p {port1 , port2, port3, port4, .......}` will scan the given ports
* `-p {start port}-{End port}` will scan range of ports starting from start port and ending at the end  port 
* `-p-` will scan all ports starting 1 to 65535 (last port)
* `--top-port={number}` will scan the top {number} of most used port
## Connect scan
```
sudo nmap {ip} -p {port} --packet-trace --disable-arp-ping -n -Pn --reason -sT 
```
* `--packet-trace` is used to track the packet sent 
* `--disable-arp-ping` is used to disable the arp pings 
* `-n` is used to disable the DNS resolution
* `-Pn` is used to disable the `ICMP ECHO ping` requests
* `--reason ` is a flag used to specify that we also need a reason for the result i.e open or closed etc..
* `-sT` is used to specify the Connect scan where we do a Three way handshake with the TCP port
## Filtered ports
* When the nmap is unable to recive the result of the network packet sent then it  is consided as `Filtered`
* Two common examples of it when done packet trace are :
```
SENT (0.0381s) TCP 10.10.14.2:60277 > 10.129.2.28:139 S ttl=47 id=14523 iplen=44  seq=4175236769 win=1024 <mss 1460>
SENT (1.0411s) TCP 10.10.14.2:60278 > 10.129.2.28:139 S ttl=45 id=7372 iplen=44  seq=4175171232 win=1024 <mss 1460>
```
* In this case we can see that the packet that was sent didn't recived any reply this simply means that our packet was rejected by the firewall and thus we aren't able to get the state of port of that device


```
SENT (0.0388s) TCP 10.129.2.28:52472 > 10.129.2.28:445 S ttl=49 id=21763 iplen=44  seq=1418633433 win=1024 <mss 1460>
RCVD (0.0487s) ICMP [10.129.2.28 > 10.129.2.28 Port 445 unreachable (type=3/code=3) ] IP [ttl=64 id=20998 iplen=72 ]
```
* In this case the firewall sent us an error code that means that we aren't able to the real state of the port as the firewall didn't allowed us to do so 
## Discovering UDP ports 
## UDP Port scan
```
sudo nmap 10.129.2.28 -F -sU 
```
* `-F` scans top 100 ports 
* `-sU ` scans for UDP ports

* As the UDP is a stateless protocol so we don't have any correct way of determing if the port is opened or closed 
* If we get a `ICMP ERROR code 3` as response then we can say the indeed the port is **closed** 
* Whereas if we get any other code then we categroiese them under **filtered|open**

* There is a `-sV`  flag which is used to get the version of the current service running
