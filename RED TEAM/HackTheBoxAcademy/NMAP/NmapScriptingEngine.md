# Nmap Scripting Engine 
* `NSE` Nmap Scripting Engine
* These Scripts are made in lua programing language 
* These scripts are catigoriesed in 14 types:
    * `auth` Determination of authentication credintials
    * `brodcast` It is used for host discovery by brodcasting and discovering hosts
    * `brute`  Excutes scripts which try to login in to services by brute forcing
    * `default` All the default scripts ran during `-sC` flag
    * `discovery` Evaluation of discovred services
    * `dos` these are used to the DOS (`Denial of service`) vuln and are used less as it may harm the service 
    * `exploit` It tries the known vuln for that  port 
    * `external` scripts used for external services for further scaning 
    * `fuzzer` It is used to identify the vuln and unexpected packet handling by the target .It takes time to execute 
    * `intrusive` These could negitively harm the target 
    * `malware` Checks if some malware infects the target system 
    * `safe` These scripts are safe and can be used by defensive side
    * `version` Extension for service detection
    * `vuln` Identification of specific vuln 
* Default scripts
```
sudo nmap <target>  -sC 
```
* Category specific scripts
```
sudo nmap <target> --script <category>
```
* Defined scripts 
```
sudo nmap <target> --scripts <script-name>,<script-name> , ...
```
* Nmap Specfying scripts
```
sudo nmap 10.129.2.28 -p 25 --script banner,smtp-commands
```
* Some more flags 
    * `-O` flag can be used to detect the OS
    * `-A` flag can be used to scan more aggressive 
    * `--traceroute` flag can be used for tracing the packet path 
    * `-sC` for scripts
## Nmap Aggreesive scan
* The aggressive scan performs **service detection**, **OS detection** and uses Default scripts on target  
* can be done by using `-A` flag 
## Vuln assesment
```
sudo nmap 10.129.2.28 -p 80 -sV --script vuln 
```

