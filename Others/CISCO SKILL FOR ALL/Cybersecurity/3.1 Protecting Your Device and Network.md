# 3.1 Protecting Your Device and Network
**You’ve probably heard of the term ‘online security.’ It’s all about taking the necessary steps to prevent your personal information from falling into the wrong hands.**
## 3.1.1 What would you do ?
* You’ve just been issued with a new laptop 
* To make your device safe and secure, you should:
	-   turn the firewall on
	-   install antivirus and antispyware
	-   manage your operating system and browser
	-   set up password protection.
## 3.1.2  Protecting Computing Devices
* Your computing devices are the portal to your online life, storing a lot of your personal data. Therefore, it’s important to protect the security of your devices.
* Som of the tips are :
	* Turn the firewall on 
		* You should use at least one type of firewall (either a software firewall or a hardware firewall on a router) to protect your device from unauthorized access. The firewall should be turned on and constantly updated to prevent hackers from accessing your personal or organization data
	* Install antivirus and antispyware
		* Malicious software, such as viruses and spyware, are designed to gain unauthorized access to your computer and your data. Once installed, viruses can destroy your data and slow down your computer. They can even take over your computer and broadcast spam emails using your account. Spyware can monitor your online activities, collect your personal information or produce unwanted pop-up ads on your web browser while you are online.
		* To prevent this, you should only ever download software from trusted websites. However, you should always use antivirus software to provide another layer of protection. This software, which often includes antispyware, is designed to scan your computer and incoming email for viruses and delete them. Keeping your software up to date will protect your computer from any new malicious software that emerges.
	* Manage your OS and browser
		* Hackers are always trying to take advantage of vulnerabilities that may exist in your operating system (such as Gnu/linux , Microsoft Windows or macOS) or web browser (such as Mozzila Firefox , Google Chrome or Apple Safari).
		* Therefore, to protect your computer and your data, you should set the security settings on your computer and browser to medium level or higher. You should also regularly update your computer’s operating system, including your web browser, and download and install the latest software patches and security updates from the vendors
	* Set up password protection
		* All of your computing devices, including PCs, laptops, tablets and smartphones, should be password protected to prevent unauthorized access. Any stored information, especially sensitive or confidential data, should be encrypted. You should only store necessary information on your mobile device, in case it is stolen or lost.
		* Remember, if any one of your devices is compromised, the criminals may be able to access all of your data through your cloud storage service provider, such as NextCloud, iCloud or Google Drive.
**IoT devices pose an even greater risk than your other computing devices. While desktop, laptop and mobile platforms receive frequent software updates, most IoT devices have their original software. If vulnerabilities are found in the software, the IoT device is likely to be vulnerable. And to make the problem worse, IoT devices require Internet access, most often relying on your local network. The result is that when IoT devices are compromised, they allow hackers access to your local network and data. The best way to protect yourself from this scenario is to set up any IoT devices on an isolated network.**
**Check out [Shodan](https://www.shodan.io/), a web-based IoT device scanner that helps you identify any vulnerable devices on the Internet.** 
## 3.1.3 Wireless Network Security at Home
* Wireless networks allow Wi-Fi enabled devices, such as laptops and tablets, to connect to the network by way of a preset network identifier, known as the service set identifier (SSID). Although a wireless router can be configured so that it doesn’t broadcast the SSID, this should not be considered adequate security for a wireless network.
* Hackers will be aware of the preset SSID and default password. Therefore, these details should be changed to prevent intruders from entering your home wireless network. Furthermore, you should encrypt wireless communication by enabling wireless security and the WPA2 encryption feature on your wireless router. But be aware, even with WPA2 encryption enabled, a wireless network can still be vulnerable.
* There was a security vulnerablity found in WPA2 protocol in 2017 
	* This vulnerability can be exploited by key reinstallation attacks (KRACKs) by intruders. In simple terms, attackers break the encryption between a wireless router and a wireless device, giving them access to network data. This flaw affects all modern, protected Wi-Fi networks.
		* To mitigate this situation, you should:
			-   update all wireless capable devices such as routers, laptops and mobile devices, as soon as security updates become available
			-   use a wired connection for any devices with a wired network interface card (NIC)
			-   use a trusted virtual private network (VPN) service when accessing a wireless network.
			- Click [here](https://www.krackattacks.com/) to find out more about KRACK.
## 3.1.4 Public Wi-Fi Risks
* When you are away from home, you can access your online information and surf the Internet via public wireless networks or Wi-Fi hotspots. However, there are some risks involved, which mean that it is best not to access or send any personal information when using public Wi-Fi.
* You should always verify that your device isn’t configured with file and media sharing and that it requires user authentication with encryption.
* You should also use an encrypted VPN service to prevent others from intercepting your information (known as ‘eavesdropping’) over a public wireless network. This service gives you secure access to the Internet, by encrypting the connection between your device and the VPN server. Even if hackers intercept a data transmission in an encrypted VPN tunnel, they will not be able to decipher it.
* Click [here](https://www.fcc.gov/consumers/guides/protecting-your-wireless-network) to find out more about protecting yourself when using wireless networks.
**Don’t forget that the Bluetooth wireless protocol, found on many smartphones and tablets, can also be exploited by hackers to eavesdrop, establish remote access controls, distribute malware and drain batteries!**
## 3.1.5 Password Security
* It’s important that all of your online accounts have a unique password. Using the same passwords leaves you and your data vulnerable to cybercriminals.
* And if it becomes too much to remember all of these passwords, you should use a password manager. This tool stores and encrypts all of your passwords and helps you log into your accounts automatically.
## 3.1.6 A strong password
* Do not use dictionary words or names in any language
* Do not use common misspellings of dictionary 
* If possible, use special characters such as ! @ # $ & * ( ) %
* Do not use computer names or account names
* Use a password with more than ten characters
## 3.1.7 Using a Passphrase
* In order to prevent unauthorized access to your devices, you should consider using passphrases instead of passwords. A passphrase generally takes the form of a sentence (‘Acat th@tlov3sd0gs.’), making it easier for you to remember. And because it’s longer than a typical password, it’s less vulnerable to dictionary or brute-force attacks.
* Few tips for creating a good passphrase
	* Choose a statement that is meaningful to you
	* Add special characters such as !@#$% * ()
	* The longer the better
	* Avoid common or famous statements, for example lyrics from a popular song
## 3.1.8 Password Guidelines
* The United States National Institute of Standards and Technology (NIST) has published improved password requirements. NIST standards are intended for government applications but can serve as a standard for other sectors as well.
* These guidelines aim to place responsibility for user verification on service providers and ensure a better experience for users overall. They state:
	-   Passwords should be at least eight characters, but no more than 64 characters.
	-   Common, easily guessed passwords, such as ‘password’ or ‘abc123’ should not be used.
	-   There should be no composition rules, such as having to include lower and uppercase letters and numbers.
	-   Users should be able to see the password when typing, to help improve accuracy.
	-   All printing characters and spaces should be allowed.
	-   There should be no password hints.
	-   There should be no password expiration period.
	-   There should be no knowledge-based authentication, such as having to provide answers to secret questions or verify transaction history.